<?php
/**
 * Certatim SeS based Mail Transport for Zend Framework Mail component
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2013 Certatim. (http://www.certatim.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Certatim\Mail\Transport;

use Zend\Mail\Transport\TransportInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mail;

class CtmSes implements TransportInterface, ServiceLocatorAwareInterface
{

    protected $serviceLotcator;
    protected $ses;

    /**
     * Injects service locator
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLotcator = $serviceLocator;
    }

    /**
     * Return the service locator injected earlier
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLotcator;
    }

    /**
     * Composes the Ses mail options from a Zend\Mail\Message and sends it to
     * Ses sendMail function
     * @param \Zend\Mail\Message $message
     */
    public function send(Mail\Message $message)
    {
        $from = $message->getFrom()->current()->toString();

        $to = array();
        foreach ($message->getTo() as $address) {
            $to[] = $address->toString();
        }

        $cc = array();
        foreach ($message->getCc() as $address) {
            $cc[] = $address->toString();
        }

        $bcc = array();
        foreach ($message->getBcc() as $address) {
            $bcc[] = $address->toString();
        }

        $replyto = array();
        foreach ($message->getReplyTo() as $address) {
            $replyto[] = $address->toString();
        }

        $mail = array(
            'Source' => $from,
            'Destination' => array(
                'ToAddresses' => $to,
                'CcAddresses' => $cc,
                'BccAddresses' => $bcc,
            ),
            'Message' => array(
                'Subject' => array(
                    'Data' => $message->getSubject(),
                ),
                'Body' => array(
                    'Html' => array(
                        'Data' => $message->getBodyText(),
                    ),
                ),
            ),
            'ReplyToAddresses' => $replyto,
        //'ReturnPath' => array(),
        );

        $this->getSes()->sendEmail($mail);
    }

    /**
     * If not already set, grabs the SesClient from the service locator
     * @return Aws\Ses\SesClient
     */
    public function getSes()
    {
        if (!isset($this->ses)) {
            $aws = $this->getServiceLocator()->get('aws');
            $this->ses = $aws->get('Ses');
        }

        return $this->ses;
    }

}
