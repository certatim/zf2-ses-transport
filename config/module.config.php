<?php
return array(
    'service_manager' => array(
        'invokables' => array(
            'Certatim\Mail\Transport\CtmSes' => 'Certatim\Mail\Transport\CtmSes',
        ),
        'aliases' => array(
            'ses_transport' => 'Certatim\Mail\Transport\CtmSes',
        ),
    ),
);
