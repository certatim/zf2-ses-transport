<?php
/**
 * Certatim Zend Framework 2 Classes (https://bitbucket.org/certatim/zf2-ses-transport)
 *
 * @link https://bitbucket.org/certatim/zf2-ses-transport for the canonical source repository
 * @copyright Copyright (c) 2013 Certatim (http://www.certatim.com)
 *
 */

namespace Certatim;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
